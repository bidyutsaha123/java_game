import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Resources {
	private static String PATH = "";

	public static class images {
		public static BufferedImage BUILDINGS;
		public static BufferedImage BACKGROUND;
		public static BufferedImage ROCKS;
		public static BufferedImage CAVES;
		public static BufferedImage TILES;
		public static BufferedImage Z1;
		public static BufferedImage Z2;
		public static BufferedImage RIGHTWALKING;
		public static BufferedImage PLAYERIDLE;
		public static BufferedImage PLAYERJUMPING;
		public static BufferedImage PLAYERDYING;
		public static BufferedImage DOWNCAVES;
		public static BufferedImage MOUNTAINS;
		public static BufferedImage PILLERS;
		public static BufferedImage LIGHT;
		public static BufferedImage ENEMY1;
		public static BufferedImage BLUST;
		public static BufferedImage GAMEINTERUPTEDACKGROUND;
		public static BufferedImage BULLET;
		public static BufferedImage WON;
		public static BufferedImage LOSS;

		public static BufferedImage STARTSCEENBACKGROUND;
		public static BufferedImage STARTGAMEBTN;
		public static BufferedImage EXITGAMEBTN;
		public static BufferedImage RESUMEGAMEBTN;
		public static BufferedImage GAMEOVERACKGROUND;
		public static BufferedImage FLAG;

		images() {
			try {
				BUILDINGS = ImageIO.read(new File(PATH + "bglayer1.png"));

				TILES = ImageIO.read(new File(PATH + "bglayer20.png"));
				CAVES = ImageIO.read(new File(PATH + "bglayer21.png"));

				Z1 = ImageIO.read(new File(PATH + "z1.png"));
				Z2 = ImageIO.read(new File(PATH + "z3.png"));
				ENEMY1 = ImageIO.read(new File(PATH + "emeny1.png"));
				RIGHTWALKING = ImageIO
						.read(new File(PATH + "rightwalking.png"));
				PLAYERIDLE = ImageIO.read(new File(PATH + "playeridle.png"));
				PLAYERJUMPING = ImageIO.read(new File(PATH
						+ "playerjumping.png"));
				PLAYERDYING = ImageIO.read(new File(PATH + "playerdying.png"));
				DOWNCAVES = ImageIO.read(new File(PATH + "bglayer25.png"));
				MOUNTAINS = ImageIO.read(new File(PATH + "mountains.png"));
				PILLERS = ImageIO.read(new File(PATH + "pillers.png"));
				LIGHT = ImageIO.read(new File(PATH + "light.png"));
				BLUST = ImageIO.read(new File(PATH + "blust.png"));
				STARTGAMEBTN = ImageIO.read(new File(PATH + "start.png"));
				EXITGAMEBTN = ImageIO.read(new File(PATH + "exit.png"));
				RESUMEGAMEBTN = ImageIO.read(new File(PATH + "resume.png"));
				GAMEOVERACKGROUND = ImageIO
						.read(new File(PATH + "gameover.png"));
				GAMEINTERUPTEDACKGROUND = ImageIO.read(new File(PATH
						+ "interupted.png"));
				WON = ImageIO.read(new File(PATH + "won.png"));
				LOSS = ImageIO.read(new File(PATH + "loss.png"));

				BULLET = ImageIO.read(new File(PATH + "bulleth.png"));

				STARTSCEENBACKGROUND = ImageIO.read(new File(PATH + "scb.png"));
				FLAG = ImageIO.read(new File(PATH + "flag.png"));

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	Resources() {
		new images();

	}
}
