import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import myGameFramework.Coordinate;
import myGameFramework.GAnimation;
import myGameFramework.GAudioPlayer;
import myGameFramework.GPlatform;
import myGameFramework.GScene;
import myGameFramework.Velocity;

public class GamePlayingSceen extends GScene implements Runnable {

	public static Boolean sceneState = false;

	Tiles tiles;
	Tiles upperCave;
	Tiles mountains;
	Tiles lowerCave;
	GAnimation zombi1;
	GAnimation zombi2;
	GAnimation light1;
	GAnimation light2;
	GAnimation light3;
	GAnimation light4;
	GAnimation flag;

	Player hero;
	public boolean isGameOver = false;
	public boolean isPlayerKilled = false;
	Enemy enemy1;
	Enemy enemy2;
	Enemy enemy3;
	private ExecutorService es = Executors.newFixedThreadPool(100);
	private ArrayList<Future<?>> futures = new ArrayList<Future<?>>();
	private static int CamXbeforeResume = 0;
	private static int CamYbeforeResume = 0;
	private GAudioPlayer bgSound1;

	public GamePlayingSceen() {

		super(Resources.images.BUILDINGS);

		upperCave = new Tiles(Resources.images.CAVES, 0, -70);
		upperCave.setLabel(0);

		lowerCave = new Tiles(Resources.images.DOWNCAVES, 0, 497);
		lowerCave.setLabel(0);
		mountains = new Tiles(Resources.images.MOUNTAINS, -200, 230);
		mountains.setLabel(0);
		mountains.setVelocity(new Velocity(4, 0));
		this.add(upperCave);
		this.add(mountains);
		this.add(lowerCave);

		tiles = new Tiles(Resources.images.TILES, 0, 10);
		tiles.setLabel(1);
		this.add(tiles);

		zombi1 = new GAnimation(Resources.images.Z1, new Coordinate(420,
				780 - 196), 344, 196, 100, 14, true);
		zombi1.setLabel(2);
		this.add(zombi1);
		zombi2 = new GAnimation(Resources.images.Z2, new Coordinate(2600,
				780 - 264), 190, 264, 100, 17, true);
		zombi2.setLabel(2);
		this.add(zombi2);
		light1 = new GAnimation(Resources.images.LIGHT, new Coordinate(00,
				780 - 350), 180, 3600, 100, 5, true);
		light1.setLabel(2);
		this.add(light1);
		light2 = new GAnimation(Resources.images.LIGHT, new Coordinate(971,
				780 - 350), 180, 3600, 100, 5, true);
		light2.setLabel(2);
		this.add(light2);
		light3 = new GAnimation(Resources.images.LIGHT, new Coordinate(2000,
				780 - 350), 180, 3600, 100, 5, true);
		light3.setLabel(2);
		this.add(light3);
		light4 = new GAnimation(Resources.images.LIGHT, new Coordinate(3000,
				780 - 350), 180, 3600, 100, 5, true);
		light4.setLabel(2);
		this.add(light4);
		flag = new GAnimation(Resources.images.FLAG, new Coordinate(2900,
				780 - 528), 1140 / 10, 209, 100, 10, true);
		flag.setLabel(2);
		this.add(flag);

		hero = new Player();
		hero.setLabel(3);
		this.add(hero);
		enemy1 = new Enemy(new Coordinate(1000, 190));
		enemy1.setLabel(4);
		this.add(enemy1);

		enemy2 = new Enemy(new Coordinate(1700, 190));
		enemy2.setLabel(4);
		this.add(enemy2);

		enemy3 = new Enemy(new Coordinate(2500, 190));
		enemy3.setLabel(4);
		this.add(enemy3);

		bgSound1 = new GAudioPlayer("03.wav", GAudioPlayer.PLAY_REPEATEDLY);

		gAudioPlayers.add(bgSound1);

	}

	public void gameOwn() {

		GPlatform.GraphicsManager.updateCamera(0, 0);
		GameOverSceen.GameValue = 1;
		this.setSceneToDeActived();
		GameOverSceen.sceneState = true;

	}

	public void gameLoss() {

		GPlatform.GraphicsManager.updateCamera(0, 0);
		GameOverSceen.GameValue = 0;
		this.setSceneToDeActived();
		GameOverSceen.sceneState = true;

	}

	public static void cameraReposition() {
		GPlatform.GraphicsManager.updateCamera(CamXbeforeResume,
				CamYbeforeResume);

	}

	public void gamePaused() {
		CamXbeforeResume = hero.location.getIntX();
		CamYbeforeResume = hero.location.getIntY();
		GPlatform.GraphicsManager.updateCamera(0, 0);

		this.setSceneToDeActived();
		GameInterruptedSceen.sceneState = true;

	}

	@Override
	public void keyPressed(KeyEvent arg0) {

		switch (arg0.getKeyCode()) {
		case KeyEvent.VK_ESCAPE:

			gamePaused();

			break;

		case KeyEvent.VK_LEFT:
			if (hero.walkLeft()) {

				mountains.updateLoacation(Tiles.RIGHT);

			}
			break;
		case KeyEvent.VK_RIGHT:
			if (hero.walkRight()) {

				mountains.updateLoacation(Tiles.LEFT);

			}

			break;
		case KeyEvent.VK_UP:
			hero.jumpingUp();
			break;
		case KeyEvent.VK_DOWN:
			hero.fallingDown();
			break;

		}

	}

	@Override
	public void update() {

		super.update();
		if (hero.isAlive())
			if (Enemy.shooter.updatecollisionResulation(hero.objectInPolygon())) {
				new GAudioPlayer("blust.wav", GAudioPlayer.PLAY_ONCE)
						.playOnce();
				hero.killing();
			}

		enemy1.update(hero);
		enemy2.update(hero);
		enemy3.update(hero);

		if (!enemy1.isDied()
				&& (hero.shooter.collisionResulation(enemy1.objectInPolygon()))) {
			new GAudioPlayer("blust.wav", GAudioPlayer.PLAY_ONCE).playOnce();
			enemy1.killing();

		}
		if (!enemy2.isDied()
				&& (hero.shooter.collisionResulation(enemy2.objectInPolygon()))) {
			new GAudioPlayer("blust.wav", GAudioPlayer.PLAY_ONCE).playOnce();
			enemy2.killing();

		}
		if (!enemy3.isDied()
				&& (hero.shooter.collisionResulation(enemy3.objectInPolygon()))) {
			new GAudioPlayer("blust.wav", GAudioPlayer.PLAY_ONCE).playOnce();
			enemy3.killing();

		}
		// checking either hero is died
		if (hero.isDied) {
			bgSound1.stopPlaying();

			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			gameLoss();
		}
		// checking for goal
		if ((hero.objectInPolygon()).intersects(flag.objectInRectangle())) {
			new GAudioPlayer("won.wav", GAudioPlayer.PLAY_ONCE).playOnce();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			gameOwn();

		}

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		switch (arg0.getKeyCode()) {

		case KeyEvent.VK_LEFT:
			hero.idleLeft();
			break;
		case KeyEvent.VK_RIGHT:
			hero.idleRight();
			break;
		case KeyEvent.VK_SPACE:
			hero.shoot();
			break;
		case KeyEvent.VK_SHIFT:

			break;
		case KeyEvent.VK_CONTROL:
			enemy1.shoot();
			break;
		case KeyEvent.VK_PRINTSCREEN:
			// enemy1.kill();
			break;

		}

	}

	@Override
	public void draw() {

		super.draw();
		Enemy.shooter.draw();

	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {

	}

	@Override
	protected Boolean isSceneActive() {

		return sceneState;
	}

	@Override
	protected void setSceneToActived() {
		sceneState = true;
		for (GAudioPlayer p : gAudioPlayers) {
			p.resume();
		}

	}

	@Override
	protected void setSceneToDeActived() {
		sceneState = false;
		for (GAudioPlayer p : gAudioPlayers) {
			p.pause();
		}

	}

	@Override
	public void run() {

		while (true) {

		}

	}

}
