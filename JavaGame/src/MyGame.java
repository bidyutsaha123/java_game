import myGameFramework.Game;

public class MyGame extends Game {

	MyGame() {
		super();

		GameStartSceen startSceen = new GameStartSceen();
		startSceen.setSceneToActived();
		this.add(startSceen);

		GamePlayingSceen gameSceen = new GamePlayingSceen();

		this.add(gameSceen);

		GameInterruptedSceen interruptedSceen = new GameInterruptedSceen();

		this.add(interruptedSceen);

		GameOverSceen gameOverSceen = new GameOverSceen(Resources.images.LOSS,
				Resources.images.WON);

		this.add(gameOverSceen);

	}
}
