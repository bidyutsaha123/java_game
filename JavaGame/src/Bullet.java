import java.awt.image.BufferedImage;

import myGameFramework.Coordinate;
import myGameFramework.GMovingComponent;
import myGameFramework.GPlatform.GraphicsManager;
import myGameFramework.Settings;
import myGameFramework.Velocity;

public class Bullet extends GMovingComponent {

	public Bullet(BufferedImage img, Coordinate location, Velocity v) {
		bgImage = img;
		this.location = location;
		velocity = v;
		this.isActive = true;
		height = bgImage.getHeight();
		width = bgImage.getWidth();

	}

	@Override
	public Boolean isActive() {
		if (isActive)
			return true;
		else
			return false;

	}

	@Override
	public void draw() {
		GraphicsManager.graphics.drawImage(bgImage, location.getIntX(),
				location.getIntY(), null);

	}

	@Override
	public void update() {
		location.update(velocity);
		if (location.Y < -getHeight()
				|| location.Y > Settings.GAMEWORLD_DIMENSION.height
						+ getHeight() || location.X < -getWidth()
				|| location.X > Settings.GAMEWORLD_DIMENSION.width + getWidth()) {
			isActive = false;
			// System.out.println("bulet deactivated");
		}

	}

}
