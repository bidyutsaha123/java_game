import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.Random;

import myGameFramework.Coordinate;
import myGameFramework.GAnimation;
import myGameFramework.GAudioPlayer;
import myGameFramework.GComponent;
import myGameFramework.Settings;
import myGameFramework.Velocity;

public class Enemy extends GComponent {
	private boolean isAlive;
	private boolean isDying;
	private boolean isDied;

	private Polygon polygone;
	private Coordinate bulletAnchor;
	public static Shooter shooter;
	private GAnimation flying;
	private GAnimation dying;
	private int bulletSpeed;

	private long lastShootingTime;

	public Enemy(Coordinate c) {
		super();

		isAlive = true;
		isDying = false;
		isDied = false;

		bulletSpeed = 30;

		location = c;
		height = 200;
		width = 2133 / 8;

		bulletAnchor = new Coordinate(getWidth() / 2, getHeight() / 2);
		int xPoly[] = { 121, 190, 126, 77 };
		int yPolly[] = { 37, 95, 154, 102 };
		polygone = new Polygon(xPoly, yPolly, xPoly.length);
		lastShootingTime = 0;

		shooter = new Shooter();

		flying = new GAnimation(Resources.images.ENEMY1, 2133 / 8, 200, 100, 8,
				true);
		dying = new GAnimation(Resources.images.BLUST, 1200 / 6, 200, 100, 6,
				false);

	}

	public void killing() {
		isAlive = false;
		isDying = true;
	}

	public boolean isCollideWith(Shape s) {
		Area myShape = new Area(objectInPolygon());
		Area otherShape = new Area(s);
		myShape.intersect(otherShape);
		return !myShape.isEmpty();
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void shoot() {
		if (isAlive) {
			new GAudioPlayer("shoot.wav", GAudioPlayer.PLAY_ONCE).playOnce();
			;

			Velocity v;
			Coordinate c;

			c = new Coordinate(bulletAnchor.getIntX() + location.X,
					bulletAnchor.getIntY() + location.Y);
			v = new Velocity(bulletSpeed, 180);

			shooter.addBullets(v, c);
		}

	}

	@Override
	public void update() {
		// isDied = dying.getSignal();

	}

	public void update(Player p) {

		// shooter.update();
		if (isAlive) {
			flying.update();
			Random r = new Random();
			{
				if (((p.location.X) + (Settings.VIEWPORT_DIMENSION.width / 2) >= this.location
						.getX()) && this.location.getIntX() > p.location.X)

					if (System.nanoTime() - lastShootingTime > Settings.SEC_IN_NANOSEC * 3)
						if (r.nextInt(2) == 1) {
							shoot();
							lastShootingTime = System.nanoTime();

						}
			}

		} else if (isDying) {
			dying.update();
			if (!dying.getSignal()) {
				isDied = true;
				isDying = false;
			}
		}

	}

	public boolean isDying() {
		return isDying;
	}

	public boolean isDied() {
		return isDied;
	}

	@Override
	public void draw() {
		// shooter.draw();
		if (isDied)
			return;

		if (isAlive) {
			flying.draw(this.location);
			return;

		}
		if (isDying)
			dying.draw(this.location);

	}

	@SuppressWarnings("unchecked")
	public static ArrayList<Bullet> getAllFiredBullets() {

		return shooter.getAllBullets();
	}

	public Polygon objectInPolygon() {
		Polygon newPolygon;

		newPolygon = new Polygon(polygone.xpoints, polygone.ypoints,
				polygone.npoints);

		newPolygon.translate(location.getIntX(), location.getIntY());
		return newPolygon;

	}
}
