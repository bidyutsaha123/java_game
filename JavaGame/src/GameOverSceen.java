import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import myGameFramework.Coordinate;
import myGameFramework.GAudioPlayer;
import myGameFramework.GButton;
import myGameFramework.GButtonClicked;
import myGameFramework.GPlatform;
import myGameFramework.GPlatform.GraphicsManager;
import myGameFramework.GScene;

public class GameOverSceen extends GScene {
	public static Boolean sceneState = false;
	public static int GameValue = 0;

	GButton exitBtn;
	BufferedImage img1;
	BufferedImage img2;

	public GameOverSceen(BufferedImage m1, BufferedImage m2) {
		super(Resources.images.GAMEOVERACKGROUND);

		exitBtn = new GButton(Resources.images.EXITGAMEBTN,
				Resources.images.EXITGAMEBTN, new Coordinate(460, 325));

		this.add(exitBtn);
		GAudioPlayer bgSound1 = new GAudioPlayer("04.wav",
				GAudioPlayer.PLAY_REPEATEDLY);

		gAudioPlayers.add(bgSound1);

	}

	public void click(MouseEvent e) {

	}

	@Override
	public void draw() {

		super.draw();
		if (GameValue == 0)
			GraphicsManager.graphics.drawImage(Resources.images.LOSS, 0, 0,
					null);
		else {
			GraphicsManager.graphics
					.drawImage(Resources.images.WON, 0, 0, null);

		}

		try {
			GraphicsManager.graphics.drawImage(
					ImageIO.read(new File("mousepointer.png")),
					GPlatform.InputManager.MouseMove.point.x,
					GPlatform.InputManager.MouseMove.point.y, null);
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		// GameScene.isSceneActive = true;
		exitBtn.click(arg0, new GButtonClicked() {

			@Override
			public void clicked_EventHandler() {
				System.exit(0);

			}
		});

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	protected Boolean isSceneActive() {
		// TODO Auto-generated method stub

		return sceneState;
	}

	@Override
	protected void setSceneToActived() {
		sceneState = true;
		for (GAudioPlayer p : gAudioPlayers) {
			p.resume();
		}

	}

	@Override
	protected void setSceneToDeActived() {
		sceneState = false;
		for (GAudioPlayer p : gAudioPlayers) {
			p.pause();
		}

	}
}
