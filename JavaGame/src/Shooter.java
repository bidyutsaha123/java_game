import java.awt.Polygon;
import java.util.ArrayList;

import myGameFramework.Coordinate;
import myGameFramework.GComponent;
import myGameFramework.Velocity;

public class Shooter {

	protected ArrayList<Bullet> bullets;

	public Shooter() {

		bullets = new ArrayList<Bullet>();

	}

	public void addBullets(Velocity v, Coordinate c) {

		bullets.add(getBullet(v, c));

		return;

	}

	public void update() {

		for (int i = 0; i < bullets.size(); i++) {

			if ((bullets.get(i)).isActive)
				(bullets.get(i)).update();
			else
				bullets.remove(i);

		}

	}

	public boolean updatecollisionResulation(Polygon p) {
		boolean isCollided = false;

		for (int i = 0; i < bullets.size(); i++) {

			if ((bullets.get(i)).isActive) {
				if ((bullets.get(i)).isColliedWith(p)) {
					System.out.println("hero enemy bullet colided in s");
					isCollided = true;
					bullets.remove(i);
				} else {
					(bullets.get(i)).update();
					System.out.println("hero enemy bullet not colided in s");
				}
			} else {
				bullets.remove(i);
			}

		}
		return isCollided;

	}

	public boolean collisionResulation(Polygon p) {
		boolean isCollided = false;

		for (int i = 0; i < bullets.size(); i++) {

			if ((bullets.get(i)).isColliedWith(p)) {
				bullets.remove(i);
				isCollided = true;
				return isCollided;

			}

		}
		return isCollided;

	}

	protected Bullet getBullet(Velocity v, Coordinate c) {
		Bullet bullet;

		bullet = new Bullet(Resources.images.BULLET, c, v);
		return bullet;

	}

	public ArrayList<Bullet> getAllBullets() {

		return bullets;

	}

	public void draw() {

		for (GComponent gc : bullets) {

			gc.draw();

		}

	}

}
