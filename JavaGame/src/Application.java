import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import myGameFramework.GPlatform;

@SuppressWarnings("serial")
public class Application extends JFrame {
	public Application() {
		this.setTitle("");
		this.setUndecorated(true);

		new Resources();

		this.setBackground(Color.yellow);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(new GPlatform(new MyGame()));
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				Application ex = new Application();
				ex.setVisible(true);

			}
		});

	}

}
