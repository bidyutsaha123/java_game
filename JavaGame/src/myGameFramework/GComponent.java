package myGameFramework;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

abstract public class GComponent extends GBase {

	public BufferedImage bgImage;
	public Coordinate location;

	public int height;
	public int width;

	/**
	 * @param backgroundImage
	 *            background Image of the object
	 * @param location
	 *            coordinate of the object in 2d
	 */
	public GComponent() {
		this.location = new Coordinate();
		bgImage = null;
		height = 0;
		width = 0;

	}

	public GComponent(BufferedImage backgroundImage, Coordinate location) {

		this.bgImage = backgroundImage;
		this.location = location;
		this.width = backgroundImage.getWidth();
		this.height = backgroundImage.getHeight();

	}

	/**
	 * 
	 * @param location
	 *            coordinate of the object in 2d
	 * @param height
	 *            height of the object in pixel
	 * @param width
	 *            height of the object in pixel
	 */
	protected GComponent(Coordinate location, int height, int width) {

		this.location = location;
		this.height = height;
		this.width = width;

	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * 
	 * @return current coordinate of the object
	 */

	protected Coordinate getLocation() {
		return location;
	}

	/**
	 * 
	 * @param location
	 *            set the coordinate of the object
	 */

	protected void setLocation(Coordinate location) {
		this.location = location;
	}

	/**
	 * 
	 * @return get get tag of the object
	 */

	protected int getHeight() {
		return height;
	}

	/**
	 * 
	 * @return width of the object in pixel
	 */
	protected int getWidth() {
		return width;
	}

	/**
	 * 
	 * @return a Rectangle equivalent of the object with respect of height,
	 *         width and coordinate
	 * 
	 */

	public Rectangle objectInRectangle() {
		return new Rectangle(new Point(this.location.getIntX(),
				this.location.getIntY()), new Dimension(this.getWidth(),
				this.getHeight()));

	}

	/**
	 * 
	 * @param r
	 *            Another object whose shape is Rectangle
	 * @return true if r is collied with this object
	 */

	public Boolean isColliedWith(GComponent obj) {
		return this.objectInRectangle().intersects(obj.objectInRectangle());

	}

	public Boolean isColliedWith(Polygon poly) {
		return poly.intersects(this.objectInRectangle());

	}

	/**
	 * 
	 * @param p
	 *            a coordinate or point in 2d
	 * @return true if the object contains this point
	 */

	protected Boolean containsPointBoolean(Point p) {
		return this.objectInRectangle().contains(p);

	}

	@Override
	public abstract void draw();

	@Override
	public abstract void update();

}
