package myGameFramework;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public abstract class GCanvas extends JPanel implements KeyListener,
		MouseListener, MouseMotionListener {
	protected BufferedImage img;
	Graphics2D g2d;

	public GCanvas() {
		super();

		this.addKeyListener(this);
		this.setFocusable(true);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		// for disappearing the cursor
		BufferedImage blankCursorImg = new BufferedImage(16, 16,
				BufferedImage.TYPE_INT_ARGB);
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
				blankCursorImg, new Point(0, 0), null);
		this.setCursor(blankCursor);

		setDoubleBuffered(true);

		img = new BufferedImage(Settings.GAMEWORLD_DIMENSION.width,
				Settings.GAMEWORLD_DIMENSION.height,
				BufferedImage.TYPE_INT_ARGB);
		g2d = img.createGraphics();

	}

	public BufferedImage GetBufferImage() {
		return img;
	}

	@Override
	protected void paintComponent(Graphics arg0) {

		Graphics2D gr = (Graphics2D) arg0;
		BufferedImage subImage = img.getSubimage(
				GPlatform.GraphicsManager.CamX, GPlatform.GraphicsManager.CamY,
				Settings.VIEWPORT_DIMENSION.width,
				Settings.VIEWPORT_DIMENSION.height);
		gr.drawImage(subImage, 0, 0, null);

	}

}