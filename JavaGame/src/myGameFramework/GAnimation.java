package myGameFramework;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class GAnimation extends GComponent {
	public static final int ORIENTATION_NORMAL = 1;
	public static final int ORIENTATION_FLIP_HORIZONTAL = 2;
	public static final int ORIENTATION_FLIP_VERTICAL = 3;
	protected int orientation;
	protected int frameWidth;
	protected int frameHight;
	protected int frameTime;
	protected int totalFrames;
	protected Boolean loop;
	private int sX1;
	private int sX2;
	private int currentFrameNo;
	private long lastFrameCreationTime;
	private boolean signal;
	private boolean showAfterComplete;
	private boolean isComplete = false;

	public GAnimation(BufferedImage backgroundImage, int fWidth, int fHeight,
			int ftime, int fNo, Boolean loop) {
		super();
		showAfterComplete = false;
		this.bgImage = backgroundImage;
		orientation = GAnimation.ORIENTATION_NORMAL;
		this.frameWidth = fWidth;
		this.frameHight = fHeight;
		this.frameTime = ftime;
		this.totalFrames = fNo;
		this.frameTime = ftime;
		this.totalFrames = fNo;
		this.loop = loop;
		currentFrameNo = 0;
		signal = true;
		// width;

	}

	public boolean isComplete() {
		return isComplete;
	}

	public boolean isShowAfterComplete() {
		return showAfterComplete;
	}

	public void setShowAfterComplete(boolean showAfterComplete) {
		this.showAfterComplete = showAfterComplete;
	}

	public GAnimation(BufferedImage backgroundImage, Coordinate c, int fWidth,
			int fHeight, int ftime, int fNo, Boolean loop) {
		super(backgroundImage, c);
		orientation = GAnimation.ORIENTATION_NORMAL;
		this.frameWidth = fWidth;
		this.frameHight = fHeight;
		this.frameTime = ftime;
		this.totalFrames = fNo;
		this.frameTime = ftime;
		this.totalFrames = fNo;
		this.loop = loop;
		currentFrameNo = 0;
		signal = true;
		// width;

	}

	@Override
	public void draw() {
		Coordinate loc = this.location;
		// GPlatform.GraphicsManager.graphics.drawRect(loc.getIntX(),
		// loc.getIntY(), frameWidth, frameHight);
		if (signal == false && showAfterComplete == false)
			return;

		switch (orientation) {

		case GAnimation.ORIENTATION_NORMAL:
			// GPlatform.GraphicsManager.graphics.drawRect(loc.getIntX(),
			// loc.getIntY(), frameWidth, frameHight);
			GPlatform.GraphicsManager.graphics.drawImage(this.bgImage,
					loc.getIntX(), loc.getIntY(), loc.getIntX()
							+ this.frameWidth, loc.getIntY() + this.frameHight,
					sX1, 0, sX2, frameHight, null);

			break;
		case GAnimation.ORIENTATION_FLIP_HORIZONTAL:
			// GPlatform.GraphicsManager.graphics.drawRect(loc.getIntX(),
			// loc.getIntY(), frameWidth, frameHight);
			GPlatform.GraphicsManager.graphics.drawImage(this.bgImage,
					loc.getIntX() + 1 * this.frameWidth, loc.getIntY(),
					loc.getIntX(), loc.getIntY() + this.frameHight, sX1, 0,
					sX2, frameHight, null);

			break;
		}
		if (signal == false)
			isComplete = true;

	}

	public void draw(Coordinate loc) {
		// GPlatform.GraphicsManager.graphics.drawRect(loc.getIntX(),
		// loc.getIntY(), frameWidth, frameHight);

		switch (orientation) {

		case GAnimation.ORIENTATION_NORMAL:
			// GPlatform.GraphicsManager.graphics.drawRect(loc.getIntX(),
			// loc.getIntY(), frameWidth, frameHight);
			GPlatform.GraphicsManager.graphics.drawImage(this.bgImage,
					loc.getIntX(), loc.getIntY(), loc.getIntX()
							+ this.frameWidth, loc.getIntY() + this.frameHight,
					sX1, 0, sX2, frameHight, null);

			break;
		case GAnimation.ORIENTATION_FLIP_HORIZONTAL:
			// GPlatform.GraphicsManager.graphics.drawRect(loc.getIntX(),
			// loc.getIntY(), frameWidth, frameHight);
			GPlatform.GraphicsManager.graphics.drawImage(this.bgImage,
					loc.getIntX() + 1 * this.frameWidth, loc.getIntY(),
					loc.getIntX(), loc.getIntY() + this.frameHight, sX1, 0,
					sX2, frameHight, null);

			break;
		}

	}

	public int getOrientation() {
		return orientation;
	}

	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	public void generateNextFrame() {

		Long curTime = System.currentTimeMillis();

		if ((curTime - lastFrameCreationTime) >= frameTime) {
			if (signal) {

				sX1 = frameWidth * currentFrameNo;
				sX2 = sX1 + frameWidth;
				currentFrameNo += 1;
				if (currentFrameNo == totalFrames) {
					currentFrameNo = 0;
					if (!loop) {
						signal = false;
					}

				}
			}

			lastFrameCreationTime = System.currentTimeMillis();
		}

	}

	@Override
	public void update() {
		generateNextFrame();

	}

	@Override
	public Rectangle objectInRectangle() {

		return new Rectangle(new Point(this.location.getIntX(),
				this.location.getIntY()), new Dimension(this.frameWidth,
				this.getHeight()));

	}

	public boolean getSignal() {
		return signal;
	}

}
