package myGameFramework;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public abstract class GScene extends GComponent implements KeyListener,
		MouseListener, MouseMotionListener {
	protected ArrayList<GComponent> gComponents;
	protected ArrayList<GAudioPlayer> gAudioPlayers;
	protected Boolean renderAble;
	protected int labelBound;
	private ExecutorService es = Executors.newFixedThreadPool(100);
	private ArrayList<Future<?>> futures = new ArrayList<Future<?>>();

	public int getLabelBound() {
		return labelBound;
	}

	public void setLabelBound(int labelBound) {
		this.labelBound = labelBound;
	}

	public GScene(BufferedImage img) {
		super(img, new Coordinate());
		gComponents = new ArrayList<GComponent>();
		gAudioPlayers = new ArrayList<GAudioPlayer>();
		renderAble = true;
		labelBound = 0;
		// this.activeScene = false;

	}

	abstract protected Boolean isSceneActive();

	abstract protected void setSceneToActived();

	abstract protected void setSceneToDeActived();

	synchronized final public void add(GComponent obj0) {
		gComponents.add(obj0);
		if (labelBound < obj0.label)
			labelBound = obj0.label;
	}

	synchronized final public void remove(GComponent obj0) {
		gComponents.remove(obj0);
	}

	@Override
	public void update() {

		if (renderAble) {

			futures.clear();
			for (GComponent obj : gComponents) {
				futures.add(es.submit(new Runnable() {

					@Override
					public void run() {
						obj.update();

					}
				}));

			}

			for (Future<?> future : futures) {
				try {
					future.get();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				} catch (ExecutionException e) {
					throw new RuntimeException(e);
				}
			}

		} else {

		}

	}

	protected void onAppeared() {
		this.setSceneToActived();

	}

	protected void onDisAppeared() {

		this.setSceneToDeActived();

	}

	protected void onPaused() {

		renderAble = false;
		for (GAudioPlayer gAudioPlayer : gAudioPlayers) {
			gAudioPlayer.pause();
		}

	}

	protected void onResumed() {
		renderAble = true;
		for (GAudioPlayer gAudioPlayer : gAudioPlayers) {
			gAudioPlayer.resume();
		}

	}

	@Override
	public void draw() {

		GPlatform.GraphicsManager.graphics.drawImage(this.bgImage, 0, 0, null);
		// two for loops is used for label wise
		if (renderAble) {

			for (int i = 0; i <= labelBound; i++) {

				futures.clear();
				for (GComponent obj : gComponents) {
					if (obj.label == i) {
						futures.add(es.submit(new Runnable() {

							@Override
							public void run() {

								obj.draw();

							}
						}));

					}

				}
				for (Future<?> future : futures) {
					try {
						future.get();
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					} catch (ExecutionException e) {
						throw new RuntimeException(e);
					}
				}

			}

		}

	}

}
