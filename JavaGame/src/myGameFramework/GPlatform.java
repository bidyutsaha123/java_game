package myGameFramework;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public final class GPlatform extends GCanvas {

	public static class InputManager {
		public static class MouseClick {
			public static Point point = new Point(0, 0);
		}

		public static class MouseMove {
			public static Point point = new Point(-100, -100);
		}

		public static class KeyState {
			public static Boolean[] isDown = new Boolean[555];

			KeyState() {
				for (int i = 0; i < 555; i++)
					isDown[i] = false;
			}
		}

		InputManager() {
			new MouseClick();
			new MouseMove();
			new KeyState();

		}

	}

	public static class GraphicsManager {
		public static Graphics2D graphics;
		public static int CamX = 0;
		public static int CamY = 0;
		public static Boolean isBusy;

		GraphicsManager(Graphics2D g2d) {
			graphics = g2d;
			isBusy = false;

		}

		public static void updateCamera(int x, int y) {

			if (x - Settings.VIEWPORT_DIMENSION.width / 2 <= 0) {
				CamX = 0;

			} else if (x + Settings.VIEWPORT_DIMENSION.width / 2 >= Settings.GAMEWORLD_DIMENSION.width) {
				CamX = Settings.GAMEWORLD_DIMENSION.width
						- Settings.VIEWPORT_DIMENSION.width;

			} else {
				CamX = x - Settings.VIEWPORT_DIMENSION.width / 2;

			}

		}

	}

	Game game;

	public GPlatform(Game game) {
		super();
		new InputManager();
		new GraphicsManager(this.g2d);

		setPreferredSize(new Dimension(Settings.VIEWPORT_DIMENSION.width,
				Settings.VIEWPORT_DIMENSION.height));
		this.game = game;
		Thread gameThread = new Thread() {
			@Override
			public void run() {
				Loop();

			}

		};
		gameThread.start();
	}

	protected void Loop() {

		// for maintaining GAME_FPS.
		long startTime, CompletingTime, timeLeft;

		while (true) {

			startTime = System.nanoTime();

			game.render();

			this.repaint();
			// adjusting the gameFPS

			CompletingTime = System.nanoTime() - startTime;

			System.out.println("Your system can render this game at   FPS : "
					+ 1000 / (CompletingTime / Settings.MILI_SEC_IN_NANOSEC));
			System.out.println("Game is running at FPS : " + Settings.GAME_FPS);
			timeLeft = (Settings.GAME_FTime - CompletingTime)
					/ Settings.MILI_SEC_IN_NANOSEC; // In milliseconds

			if (timeLeft > 0) {
				try {

					Thread.sleep(timeLeft);
				} catch (InterruptedException ex) {
				}

			}

		}

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		game.mouseClicked(arg0);
		// p = new GAudioPlayer("a.wav");
		// p.playOnce();

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		InputManager.MouseMove.point = new Point(-100, -100);
		// game.mouseEntered(arg0);
		// p.resume();

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		InputManager.MouseMove.point = new Point(-100, -100);
		game.mouseExited(arg0);
		// p.pause();

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		game.mousePressed(arg0);

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		game.mousePressed(arg0);

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		game.mouseDragged(arg0);

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {

		InputManager.MouseMove.point = arg0.getPoint();
		game.mouseMoved(arg0);

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		InputManager.KeyState.isDown[arg0.getKeyCode()] = true;
		game.keyPressed(arg0);

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		InputManager.KeyState.isDown[arg0.getKeyCode()] = false;
		game.keyReleased(arg0);

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		game.keyTyped(arg0);

	}

}
