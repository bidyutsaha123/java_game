package myGameFramework;

public class Velocity {
	public float speed;
	public float angle;

	/**
     * 
     */
	public Velocity() {
		speed = 0;
		angle = 0;
	}

	/**
	 * 
	 * @param speed
	 *            pixel per second
	 * @param angle
	 *            move towards the angle ( in degree )
	 */
	public Velocity(float speed, float angle) {
		this.speed = speed;
		this.angle = angle;
	}

}
