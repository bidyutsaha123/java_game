package myGameFramework;

public class Coordinate {
	public float X;
	public float Y;

	public Coordinate() {
		X = 0;
		Y = 0;
	}

	public Coordinate(float x, float y) {
		X = x;
		Y = y;
	}

	public float getX() {
		return X;
	}

	public int getIntX() {
		return (int) X;
	}

	public float getY() {
		return Y;
	}

	public int getIntY() {
		return (int) Y;
	}

	public void setX(float x) {
		X = x;
	}

	public void setY(float y) {
		Y = y;
	}

	public void update(Coordinate l) {
		X += l.X;
		Y += l.Y;
	}

	public void update(Velocity v) {
		X += v.speed * Math.cos((Math.PI / 180) * v.angle);
		Y += v.speed * Math.sin((Math.PI / 180) * v.angle);
	}

}
