package myGameFramework;

public abstract class GBase {

	public int label;
	public Boolean isActive;
	public String Tag;

	public GBase() {

		this.isActive = true;

		this.Tag = "";
		this.label = 0;

		System.out.println("Class : <" + this.getClass().getName()
				+ " > is created.");

	}

	public GBase(Coordinate location) {

		this.isActive = true;

		this.Tag = "";
		System.out.println("Class : <" + this.getClass().getName()
				+ "> is created.");

	}

	protected Boolean isActive() {
		return isActive;

	}

	protected String getTag() {
		return Tag;
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}

	protected void setTag(String tag) {
		Tag = tag;

	}

	public abstract void draw();

	public abstract void update();

}
