package myGameFramework;

import java.awt.Dimension;

public class Settings {

	static public final long SEC_IN_NANOSEC = 1000000000L;
	static public final long MILI_SEC_IN_NANOSEC = 1000000L;
	static public Dimension VIEWPORT_DIMENSION = new Dimension(1380, 780);
	static public Dimension GAMEWORLD_DIMENSION = new Dimension(3200, 800);
	static public int GAME_FPS = 12;
	static public long GAME_FTime = SEC_IN_NANOSEC / GAME_FPS; // in nanoSecend
	public static int cameraX = 0;
	public static int cameraY = 0;

}
