package myGameFramework;

import java.awt.image.BufferedImage;

abstract public class GMovingComponent extends GComponent {
	protected Velocity velocity;

	public GMovingComponent(BufferedImage backgroundImage, Coordinate location,
			Velocity velocity) {
		super(backgroundImage, location);
		this.velocity = velocity;

	}

	public GMovingComponent() {

	}

	public Velocity getVelocity() {
		return velocity;
	}

	public void setVelocity(Velocity velocity) {
		this.velocity = velocity;
	}

}
