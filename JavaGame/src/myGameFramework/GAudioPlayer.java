package myGameFramework;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class GAudioPlayer {
	public static final int PLAY_REPEATEDLY = 1;
	public static final int PLAY_ONCE = 2;

	AudioInputStream audio;
	public Clip clip;
	public int playerType;

	/**
	 * 
	 * @param audio
	 *            AudioInputStream [ Help : AudioSystem.getAudioInputStream(new
	 *            File("D:/MusicPlayer/fml.mp3").getAbsoluteFile()); ]
	 */
	public GAudioPlayer(String soundPath, int type) {
		try {
			this.audio = AudioSystem.getAudioInputStream(new File(soundPath));
			playerType = type;
		} catch (UnsupportedAudioFileException | IOException e1) {
			e1.printStackTrace();
		}
		try {
			clip = AudioSystem.getClip();
		} catch (LineUnavailableException e) {

			e.printStackTrace();
		}
		try {
			clip.open(audio);
		} catch (LineUnavailableException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void playOnce() {
		if (!clip.isRunning())
			clip.setFramePosition(0);
		clip.start();

	}

	public void pause() {

		stopPlaying();

	}

	public void resume() {
		if (playerType == GAudioPlayer.PLAY_ONCE)
			clip.start();
		else
			clip.loop(Clip.LOOP_CONTINUOUSLY);

	}

	public void playRepeatedly() {
		clip.setFramePosition(0);
		clip.loop(Clip.LOOP_CONTINUOUSLY);

	}

	public void stopPlaying() {
		if (clip.isOpen())
			clip.stop();

	}

}
