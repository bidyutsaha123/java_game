package myGameFramework;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public abstract class Game implements KeyListener, MouseListener,
		MouseMotionListener {
	protected ArrayList<GScene> scenes;

	public Game() {
		scenes = new ArrayList<GScene>();
	}

	public void render() {

		for (GScene scene : scenes) {

			if (scene.isSceneActive()) {
				scene.setSceneToActived();

				scene.update();
				scene.draw();
			}

		}

	}

	synchronized final public void add(GScene scene) {
		scenes.add(scene);
	}

	synchronized final public void remove(GScene scene) {
		scenes.remove(scene);
	}

	@Override
	final public void mouseDragged(MouseEvent arg0) {
		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.mouseDragged(arg0);
			}
		}

	}

	@Override
	final public void mouseMoved(MouseEvent arg0) {
		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.mouseMoved(arg0);

			}
		}

	}

	@Override
	final public void mouseClicked(MouseEvent arg0) {
		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.mouseClicked(arg0);

			}
		}

	}

	@Override
	final public void mouseEntered(MouseEvent arg0) {
		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.mouseEntered(arg0);

			}
		}

	}

	@Override
	final public void mouseExited(MouseEvent arg0) {
		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.mouseExited(arg0);

			}
		}

	}

	@Override
	final public void mousePressed(MouseEvent arg0) {
		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.mousePressed(arg0);

			}
		}

	}

	@Override
	final public void mouseReleased(MouseEvent arg0) {
		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.mouseReleased(arg0);

			}
		}

	}

	@Override
	final public void keyPressed(KeyEvent arg0) {
		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.keyPressed(arg0);

			}
		}

	}

	@Override
	final public void keyReleased(KeyEvent arg0) {

		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.keyReleased(arg0);

			}
		}

	}

	@Override
	final public void keyTyped(KeyEvent arg0) {
		for (GScene scene : scenes) {
			if (scene.isSceneActive()) {
				scene.keyTyped(arg0);

			}
		}

	}

}
