package myGameFramework;

import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class GButton extends GComponent {
	BufferedImage imgOnHover;

	// give same dimension pictures
	public GButton(BufferedImage buttonImage, BufferedImage buttonHoverImage,
			Coordinate location) {
		super(buttonImage, location);
		imgOnHover = buttonHoverImage;

	}

	@Override
	public void draw() {
		if (this.containsPointBoolean(GPlatform.InputManager.MouseMove.point)) {

			GPlatform.GraphicsManager.graphics.drawImage(this.imgOnHover,
					this.location.getIntX(), this.location.getIntY(), null);
		} else
			GPlatform.GraphicsManager.graphics.drawImage(this.bgImage,
					this.location.getIntX(), this.location.getIntY(), null);

	}

	public void click(MouseEvent e, GButtonClicked c) {

		if (this.containsPointBoolean(e.getPoint())) {
			c.clicked_EventHandler();
		}
	}

	@Override
	public void update() {

	}

}
