import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import myGameFramework.Coordinate;
import myGameFramework.GMovingComponent;
import myGameFramework.GPlatform.GraphicsManager;
import myGameFramework.Velocity;

public class Tiles extends GMovingComponent {
	public static final int LEFT = -1;
	public static final int RIGHT = 1;

	public Tiles(BufferedImage img, int x, int y) {

		this.bgImage = img;
		this.height = bgImage.getHeight();
		this.width = bgImage.getWidth();
		this.location = new Coordinate(x, y);
		this.velocity = new Velocity(0, 0);

	}

	@Override
	public void draw() {

		// GraphicsManager.graphics.drawImage(bgImage,
		// Settings.cameraX - location.getIntX(), Settings.cameraY
		// - location.getIntY(), null);
		// GraphicsManager.graphics
		// .drawImage(bgImage, location.getIntX() - Settings.cameraX,
		// location.getIntY() - Settings.cameraY, null);
		GraphicsManager.graphics.drawImage(bgImage, location.getIntX(),
				location.getIntY(), null);
		// System.out.println(Resources.images.LIGHT.getHeight());
		// System.out.println(location.getIntX());

	}

	@Override
	public void update() {

	}

	public void updateLoacation(int direction) {
		location.X += (velocity.speed) * direction;

	}

	public Rectangle myRect() {
		return new Rectangle(new Dimension(width, height));

	}

}
