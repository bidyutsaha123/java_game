import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import myGameFramework.Coordinate;
import myGameFramework.GAnimation;
import myGameFramework.GAudioPlayer;
import myGameFramework.GComponent;
import myGameFramework.GPlatform.GraphicsManager;
import myGameFramework.Settings;
import myGameFramework.Velocity;

public class Player extends GComponent {
	private int orientation;
	public static final int ORIENTATION_RIGHT = 1;
	public static final int ORIENTATION_LEFT = 2;
	private int state;
	public static final int STATE_WALKING = 2;
	public static final int STATE_IDLE = 3;
	public static final int STATE_JUMPING_UP = 5;
	public static final int STATE_FALLING_DOWN = 6;
	private boolean isAlive;
	public boolean isDying;
	public boolean isDied;

	private int baseY;
	private int maxJ;
	private int xSpeed;
	private int ySpeed;
	private int bulletSpeed;
	private Polygon rightOrientationPolygone;
	private Polygon leftOrientationPolygone;
	private Coordinate bulletAnchor;
	public Shooter shooter;
	private GAnimation walking;
	private GAnimation dying;
	private BufferedImage idle;
	private BufferedImage jumping;

	private GAudioPlayer jumpingSound;
	private GAudioPlayer fallingSound;
	private GAudioPlayer walkingSound;

	public boolean isDied() {
		return isDied;
	}

	public Player() {
		super();
		orientation = Player.ORIENTATION_RIGHT;
		state = Player.STATE_IDLE;
		isAlive = true;
		isDying = false;
		isDied = false;
		xSpeed = 7;
		ySpeed = 15;
		bulletSpeed = 30;
		this.maxJ = 200;
		location = new Coordinate(200, 190);
		height = 262;
		width = 260;
		baseY = location.getIntY();

		bulletAnchor = new Coordinate(223, 97);
		int xPoly[] = { 120, 146, 138, 160, 222, 158, 161, 132, 161, 100, 82,
				85, 51, 64, 74, 76, 57, 54, 95 };
		int yPolly[] = { 19, 37, 68, 95, 101, 129, 139, 163, 198, 234, 232,
				257, 258, 213, 192, 148, 143, 79, 57 };
		rightOrientationPolygone = new Polygon(xPoly, yPolly, xPoly.length);
		int flipXPoly[] = new int[xPoly.length];
		for (int i = 0; i < xPoly.length; i++) {
			flipXPoly[i] = getWidth() - xPoly[i];
		}
		leftOrientationPolygone = new Polygon(flipXPoly, yPolly,
				flipXPoly.length);

		shooter = new Shooter();
		walking = new GAnimation(Resources.images.RIGHTWALKING, 260, 262, 100,
				3, true);
		dying = new GAnimation(Resources.images.PLAYERDYING, 260, 262, 400, 3,
				false);
		idle = Resources.images.PLAYERIDLE;
		jumping = Resources.images.PLAYERJUMPING;
		setWidth(262);

		jumpingSound = new GAudioPlayer("fall.wav", GAudioPlayer.PLAY_ONCE);
		fallingSound = new GAudioPlayer("fall.wav", GAudioPlayer.PLAY_ONCE);
		walkingSound = new GAudioPlayer("walk.wav", GAudioPlayer.PLAY_ONCE);

	}

	public void killing() {
		isAlive = false;
		isDying = true;
	}

	public boolean isCollideWith(Shape s) {
		Area myShape = new Area(objectInPolygon());
		Area otherShape = new Area(s);
		myShape.intersect(otherShape);
		return !myShape.isEmpty();
	}

	public void shoot() {
		if (isAlive) {
			new GAudioPlayer("shoot.wav", GAudioPlayer.PLAY_ONCE).playOnce();

			Velocity v = null;
			Coordinate c = null;
			switch (orientation) {
			case Player.ORIENTATION_RIGHT:
				c = new Coordinate(bulletAnchor.getIntX() + location.X,
						bulletAnchor.getIntY() + location.Y);
				v = new Velocity(bulletSpeed, 0);
				break;
			case Player.ORIENTATION_LEFT:
				c = new Coordinate(getWidth() - 257 + location.X,
						bulletAnchor.getIntY() + location.Y);
				v = new Velocity(bulletSpeed, 180);
				break;
			}

			shooter.addBullets(v, c);
		}

	}

	public boolean walkLeft() {
		if (isAlive) {
			walkingSound.playOnce();

			orientation = Player.ORIENTATION_LEFT;
			state = Player.STATE_WALKING;
			walking.setOrientation(GAnimation.ORIENTATION_FLIP_HORIZONTAL);
			GraphicsManager
					.updateCamera(location.getIntX() + getWidth() / 2, 0);
			walkingSound.resume();
			if (location.getIntX() > 0)
				location.X -= xSpeed;
			else
				return false;

			return true;
		}
		return false;
	}

	public void idleLeft() {
		orientation = Player.ORIENTATION_LEFT;
		state = Player.STATE_IDLE;

	}

	public boolean walkRight() {
		if (isAlive) {

			orientation = Player.ORIENTATION_RIGHT;
			state = Player.STATE_WALKING;
			walking.setOrientation(GAnimation.ORIENTATION_NORMAL);
			GraphicsManager
					.updateCamera(location.getIntX() + getWidth() / 2, 0);

			if (location.getIntX() + getWidth() < Settings.GAMEWORLD_DIMENSION.width) {
				walkingSound.playOnce();
				location.X += xSpeed;
				walkingSound.resume();
			} else
				return false;

			return true;
		}
		return false;

	}

	public void idleRight() {
		orientation = Player.ORIENTATION_RIGHT;

		state = Player.STATE_IDLE;

	}

	public void jumpingUp() {
		if (isAlive) {

			state = Player.STATE_JUMPING_UP;
			if (baseY - location.getIntY() <= maxJ) {
				jumpingSound.playOnce();
				location.Y -= ySpeed;
				jumpingSound.resume();
			} else
				fallingDown();

		}

	}

	public void fallingDown() {

		if (baseY > location.getIntY()) {

			fallingSound.playOnce();
			location.Y += ySpeed;
			fallingSound.resume();
			if (baseY == location.getIntY()) {
				new GAudioPlayer("fall.wav", GAudioPlayer.PLAY_ONCE).playOnce();
				state = Player.STATE_IDLE;

			}
		} else {

		}

	}

	@Override
	public void update() {

		// gravity falling
		if (location.getIntY() < baseY)
			fallingDown();

		shooter.update();
		if (isAlive) {
			switch (state) {
			case Player.STATE_WALKING:
				walking.update();
				break;

			}
		} else if (isDying) {
			dying.update();
			if (!dying.getSignal()) {
				isDied = true;
				isDying = false;
			}
		}

	}

	@Override
	public void draw() {

		shooter.draw();
		if (isAlive) {
			switch (state) {
			case Player.STATE_IDLE:
				switch (orientation) {
				case Player.ORIENTATION_RIGHT:
					GraphicsManager.graphics.drawImage(idle,
							location.getIntX(), location.getIntY(), null);
					break;
				case Player.ORIENTATION_LEFT:
					GraphicsManager.graphics.drawImage(idle,
							location.getIntX(), location.getIntY(),
							location.getIntX() + getWidth(), location.getIntY()
									+ getHeight(), idle.getWidth(), 0, 0,
							idle.getHeight(), null);
					break;

				}
				break;
			case Player.STATE_FALLING_DOWN:
			case Player.STATE_JUMPING_UP:
				switch (orientation) {
				case Player.ORIENTATION_RIGHT:
					GraphicsManager.graphics.drawImage(jumping,
							location.getIntX(), location.getIntY(), null);
					break;
				case Player.ORIENTATION_LEFT:
					GraphicsManager.graphics.drawImage(jumping,
							location.getIntX(), location.getIntY(),
							location.getIntX() + getWidth(), location.getIntY()
									+ getHeight(), idle.getWidth(), 0, 0,
							idle.getHeight(), null);
					break;

				}
				break;

			case Player.STATE_WALKING:
				walking.draw(this.location);
				// GraphicsManager.graphics.fillPolygon(objectInPolygon());

				break;

			}
		} else if (isDying) {
			dying.draw(this.location);
		}

	}

	@SuppressWarnings("unchecked")
	public ArrayList<Bullet> getAllFiredBullets() {
		ArrayList<Bullet> tempBulletList;
		synchronized (this) {

			tempBulletList = (ArrayList<Bullet>) shooter.getAllBullets()
					.clone();

		}
		return tempBulletList;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public Polygon objectInPolygon() {
		Polygon newPolygon;

		if (orientation == Player.ORIENTATION_LEFT)

			newPolygon = new Polygon(leftOrientationPolygone.xpoints,
					leftOrientationPolygone.ypoints,
					leftOrientationPolygone.npoints);

		else
			newPolygon = new Polygon(rightOrientationPolygone.xpoints,
					rightOrientationPolygone.ypoints,
					rightOrientationPolygone.npoints);

		newPolygon.translate(location.getIntX(), location.getIntY());
		return newPolygon;

	}
}
